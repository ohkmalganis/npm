import {Component, Output} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {

  public try:number=1;
  title = 'Sudoku';
  public note:boolean = false;
  public start:boolean = false;
  public trucos:boolean = false;
  public fornew:boolean = false;
  public name:string;

  constructor(){
    this.name = '';
  }

  howto()
  {
    this.note = !this.note;
  }

  startnew()
  {
    localStorage.clear();
    this.fornew = true;
  }

  plusOne(value:any)
  {
    this.try = value;
  }

  saveandstart()
  {
    this.fornew = !this.fornew;
    localStorage.setItem(this.name, '1');
    this.start = !this.start;
  }

  trucosc()
  {
    this.trucos = !this.trucos;
  }


}
