/* Core Imports */
import { Component, OnInit, Input } from '@angular/core';
import { Output, EventEmitter } from '@angular/core';

/* Classes */
import { Game } from '../game';

@Component({
  selector: 'app-sudoku-container',
  templateUrl: './sudoku-container.component.html',
  styleUrls: ['./sudoku-container.component.scss']
})
export class SudokuContainerComponent implements OnInit {

  game: Game;
  hintsEnabled: boolean;
  @Output() count: EventEmitter<Object>;
  @Input() name:any;

  constructor() {
    this.hintsEnabled = false;
    this.game = new Game();
    this.count = new EventEmitter();
  }

  ngOnInit() { }

  _reset = () => {
    this.game.reset();
    let conteo = parseInt(localStorage.getItem(this.name));
    conteo = conteo + 1;
    this.count.emit(
      conteo
    );
    localStorage.setItem(this.name, conteo.toString());
  }

  _new = () => {
    console.log('Not yet implemented');
  }

  _toggleHints = () => {
    this.hintsEnabled = !this.hintsEnabled;
  }

}
